import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Typography,
  List,
  ListItem,
  ListItemIcon
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'left',
    minHeight: 'fit-content'
  },
  avatar: {
    width: 40,
    height: 40,
    alignItems: 'left'
  },
  name: {
    marginTop: theme.spacing(1)
  }
}));

const Profile = (props) => {
  const { className, ...rest } = props;

  const classes = useStyles();

  const user = {
    name: 'Shen Zhi',
    avatar: '/images/avatars/avatar_11.png',
    bio: 'Brain Director'
  };

  return (
    <div {...rest} className={clsx(classes.root, className)}>
      <List>
        <ListItem button component={RouterLink} to="/settings">
          <ListItemIcon>
            <Avatar
              alt="Person"
              className={classes.avatar}
              src={user.avatar}
            />
          </ListItemIcon>
          <Typography className={classes.name} variant="h5">
            {user.name}
          </Typography>
        </ListItem>
      </List>
    </div>
  );
};

Profile.propTypes = {
  className: PropTypes.string
};

export default Profile;
